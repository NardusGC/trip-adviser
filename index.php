<?php
require __DIR__ . '/vendor/autoload.php';

$remoteData = json_decode(file_get_contents('php://input'),true);
$boardingCardsData = require 'entity.php';
if(!empty($remoteData)) {
    $boardingCardsData['boardingCards'] = $remoteData;
}
$sorter = new Sorter($boardingCardsData);
$results = $sorter->sortBoardingCards();
print_r(json_encode($results));