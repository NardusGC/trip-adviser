#Boarding Pass filter

The application allows you to enter any number of boarding passes in any order as long as the
trips follow each other, and it will return an order list with directions from starting point to destination.  

## Getting Started

### Installation 

Download zip file and extract to site root folder.
In the root folder run the following commands.
I will assume you have already installed composer and PHPUnit
```
Composer install
```

### Docs 
To generate the PHP Docs run the following command

```
vendor/bin/phpdoc -d ./Api/ -t ./docs/api
```

In ```.entity.php``` you will find default setup of boarding cards and format details for transport types. 
For testing purposes this is used automatically when you you access the site, without making a formal request.

### Testing

The same data s mentioned above is used in ```Tests/testData.php``` as dummy data in the tests. No Need to change this. 


To run the tests run the following command:
```
./vendor/bin/phpunit Tests/ApiTest
```

## Calling the API. 

You can do a normal get request with the following json
It contains an array of boarding cards. 
There is no limit on the amount of boarding cards you can 
add it will also take them in any order you wish as long as there is always an unbroken chain.
There are some preset formatting options for each transportation type, 
if it cannot find te transportation type it will use a default layout for the list 
```
[
		{
			"transport": "train",
			"destination": "Barcelona",
			"origin": "Madrid",
			"seat": "45B",
			"gate": "",
			"number": "78A",
			"info": ""
		},
		{
			"transport": "flight",
			"destination": "Stockholm",
			"origin": "Gerona Airport",
			"seat": "3A",
			"gate": "45B",
			"number": "SK455",
			"info": "Baggage drop at ticket counter 344"
		},
		{
			"transport": "airport_bus",
			"destination": "Gerona Airport",
			"origin": "Barcelona",
			"seat": "",
			"gate": "",
			"number": "",
			"info": ""
		},
		{
			"transport": "flight",
			"destination": "New York JFK",
			"origin": "Stockholm",
			"seat": "7B",
			"gate": "22",
			"number": "SK22",
			"info": "Baggage will we automatically transferred from your last leg"
		}
]
```
This will produce the following result 
```
[
	"Take train 78A from Madrid to Barcelona. Sit in seat 45B",
	"Take the Airport Bus from Barcelona to Gerona Airport. No seat assignment",
	"From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A.  Baggage drop at ticket counter 344",
	"From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B.  Baggage will we automatically transferred from your last leg",
	"You have arrived at your final destination"
]
```