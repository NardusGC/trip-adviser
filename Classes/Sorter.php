<?php

use Api\Api;
use Api\Structures\BoardingCards;
use Api\Structures\FormatDetails;

/**
 * Class Sorter
 */
class Sorter
{
    /**
     * @var Api
     */
    protected $api;

    /**
     * Sorter constructor.
     * @param array $boardingCards
     */
    public function __construct(array $boardingCards)
    {
        $boardingCardsObject = new BoardingCards($boardingCards['boardingCards']);
        $formatDetails = new FormatDetails($boardingCards['formatDetails']);
        $this->api = new Api($boardingCardsObject, $formatDetails);
    }

    /**
     * Handle sorting of boarding Cards
     */
    public function sortBoardingCards()
    {
        $this->api->getStartPoint();
        $this->api->getEndPoint();
        $this->api->orderCards();
        $this->api->generateList();

        return $this->api->getList();
    }
}