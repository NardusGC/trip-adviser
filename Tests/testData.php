<?php
return [
    'boardingCards' => [

        [
            'transport' => 'train',
            'destination' => 'Barcelona',
            'origin' => 'Madrid',
            'seat' => '45B',
            'gate' => '',
            'number' => '78A',
            'info' => ''
        ],
        [
            'transport' => 'flight',
            'destination' => 'Stockholm',
            'origin' => 'Gerona Airport',
            'seat' => '3A',
            'gate' => '45B',
            'number' => 'SK455',
            'info' => 'Baggage drop at ticket counter 344'
        ],
        [
            'transport' => 'airport_bus',
            'destination' => 'Gerona Airport',
            'origin' => 'Barcelona',
            'seat' => '',
            'gate' => '',
            'number' => '',
            'info' => ''
        ],
        [
            'transport' => 'flight',
            'destination' => 'New York JFK',
            'origin' => 'Stockholm',
            'seat' => '7B',
            'gate' => '22',
            'number' => 'SK22',
            'info' => 'Baggage will we automatically transferred from your last leg'
        ],
    ],
    'formatDetails' => [
        'train' => 'Take train {number} from {origin} to {destination}. Sit in seat {seat}',
        'flight' => 'From {origin}, take flight {number} to {destination}. Gate {gate}, seat {seat}.  {info}',
        'airport_bus' => 'Take the Airport Bus from {origin} to {destination}. No seat assignment',
        'default' => 'Take {transport} {number} from {origin} to {destination}. {info}',
        'final' => 'You have arrived at your final destination'
    ],
    'results' => [
        "Take train 78A from Madrid to Barcelona. Sit in seat 45B",
        "Take the Airport Bus from Barcelona to Gerona Airport. No seat assignment",
        "From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A.  Baggage drop at ticket counter 344",
        "From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B.  Baggage will we automatically transferred from your last leg",
        "You have arrived at your final destination"
    ]
];