<?php
require __DIR__ . '/../vendor/autoload.php';

use Api\Api;
use Api\Structures\BoardingCards;
use Api\Structures\FormatDetails;
use PHPUnit\Framework\TestCase;

/**
 * Class ApiTest
 */
class ApiTest extends TestCase
{

    public function testCanCallApi()
    {
        $testData = require 'testData.php';

        $boardingCardsObject = new BoardingCards($testData['boardingCards']);
        $formatDetails = new FormatDetails($testData['formatDetails']);

        $api = new Api($boardingCardsObject, $formatDetails);

        $api->getStartPoint();
        $api->getEndPoint();
        $api->orderCards();
        $api->generateList();

        $results = $api->getList();

        $this->assertEquals($testData['results'],$results,'Oops something broke, the outputs do not match');
    }
}