<?php

namespace Base;
/**
 * Class BaseApi
 */
abstract class BaseApi
{

    /**
     * BaseApi constructor.
     * @param $boardingCards
     * @param $formatDetails
     */
    public function __construct($boardingCards, $formatDetails){}
    /**
     * Determine the first stop from the boarding cards
     */
    public function getStartPoint(){}

    /**
     * Determine the last stop from the boarding cards
     */
    public function getEndPoint(){}

    /**
     * Start Ordering boarding Cards
     */
    public function orderCards(){}

    /**
     * Orders the boarding cards by destination
     * @param $startPoint
     */
    protected function order($startPoint){}

    /**
     * Generate the final list to be returned by applying the card details to some formatted text
     */
    public function generateList(){}

    /**
     * Returns completed travel list
     */
    public function getList(){}

    /**
     * @param $items
     * @param $origin
     * @return bool
     */
    protected function hasDestination($items, $origin)
    {

        foreach ($items as $item) {
            if ($item->destination == $origin) {
                return false;
            }
        }

        return $origin;
    }

    /**
     * @param $items
     * @param $destination
     * @return bool
     */
    protected function hasOrigin($items, $destination)
    {

        foreach ($items as $item) {
            if ($item->origin == $destination) {
                return false;
            }
        }

        return $destination;
    }
}