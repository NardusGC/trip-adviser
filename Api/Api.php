<?php
/**
 * File Contains API class
 *
 */

namespace Api;

use Api\Structures\BoardingCards;
use Api\Structures\FormatDetails;
use Base\BaseApi;

/**
 * Class Api
 *
 * @api
 */
class Api extends BaseApi
{
    /**
     * List of boarding cards.
     * @package BoardingCards
     */
    public $boardingCards;

    /**
     * Start point for travels
     * @var bool.
     */
    private $startPoint;

    /**
     * End point for travels
     * @var bool.
     */
    private $endPoint;

    /**
     * List of boarding cards ordered
     * @var [].
     */
    private $orderedList;

    /**
     * Contains the format details for the return values
     * @var [].
     */
    private $formatDetails;

    /**
     * Completed travel details to be returned
     * @var [].
     */
    private $finalTravelDetails;

    /**
     * Api constructor.
     * @param $boardingCards BoardingCards
     * @param $formatDetails FormatDetails
     */

    public function __construct(BoardingCards $boardingCards, FormatDetails $formatDetails)
    {
        $this->boardingCards = $boardingCards;
        $this->formatDetails = $formatDetails;
    }

    /**
     * Determine Start Point.
     */
    public function getStartPoint()
    {
        foreach ($this->boardingCards->cards as $item) {
            if ($startPoint = $this->hasDestination($this->boardingCards->cards, $item->origin)) {
                $this->startPoint = $startPoint;
            }
        }

    }

    /**
     * Determine End Point.
     */
    public function getEndPoint()
    {
        foreach ($this->boardingCards->cards as $item) {
            if ($endPoint = $this->hasOrigin($this->boardingCards->cards, $item->destination)) {
                $this->endPoint = $endPoint;
            }
        }

    }

    /**
     * Order Boarding Cards.
     */
    public function orderCards()
    {
        $this->order($this->startPoint);
    }

    /**
     * Order list based on starting point.
     * @param $startPoint
     * @return array
     */
    protected function order($startPoint)
    {
        foreach ($this->boardingCards->cards as $boardingCard) {
            if ($boardingCard->origin === $startPoint) {
                $this->orderedList[] = $boardingCard;
                if ($boardingCard->destination !== $this->endPoint) {
                    $this->order($boardingCard->destination);
                }
            }
        }
    }

    /**
     * Build final results.
     */
    public function generateList()
    {
        foreach ($this->orderedList as $items) {
            $formatDetails = isset($this->formatDetails->{$items->transport}) ? $this->formatDetails->{$items->transport} : $this->formatDetails->default;
            foreach ($items as $key => $item) {
                $formatDetails = str_replace('{' . $key . '}', $item, $formatDetails);
            }
            $this->finalTravelDetails[] = $formatDetails;
            if ($items->destination === $this->endPoint) {
                $this->finalTravelDetails[] = $this->formatDetails->final;
            }
        }
    }

    /**
     * Return the completed list.
     * @return mixed
     */
    public function getList()
    {
        return $this->finalTravelDetails;
    }
}