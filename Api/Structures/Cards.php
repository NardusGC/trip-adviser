<?php

namespace Api\Structures;

/**
 * Class Cards
 * @package Structures
 */
class Cards
{
    /**
     * @var string
     */
    public $transport;

    /**
     * @var string
     */
    public $destination;

    /**
     * @var string
     */
    public $origin;

    /**
     * @var string
     */
    public $seat;

    /**
     * @var $string
     */
    public $gate;

    /**
     * @var $string
     */
    public $number;

    /**
     * @var string
     */
    public $info;

    /**
     * Cards constructor.
     * @param $cardDetails
     */
    public function __construct($cardDetails)
    {
        foreach ($cardDetails as $key => $detail) {
            $this->{$key} = $detail;
        }
    }
}