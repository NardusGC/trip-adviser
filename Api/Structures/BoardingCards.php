<?php

namespace Api\Structures;

/**
 * Class BoardingCards
 * @package Structures
 */
class BoardingCards
{
    /**
     * @var [] Cards
     */
    public $cards;

    /**
     * BoardingCards constructor.
     * @param $boardingCards
     */
    public function __construct($boardingCards)
    {
        foreach ($boardingCards as $card) {
            $this->cards[] = new Cards($card);
        }
    }
}