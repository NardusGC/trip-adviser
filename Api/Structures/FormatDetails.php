<?php

namespace Api\Structures;

/**
 * Class FormatDetails
 * @package Structures
 */
class FormatDetails
{
    /**
     * @var string
     */
    public $train;

    /**
     * @var string
     */
    public $flight;

    /**
     * @var string
     */
    public $airport_bus;

    /**
     * @var
     */
    public $default;

    /**
     * @var string
     */
    public $final;

    /**
     * FormatDetails constructor.
     * @param $details
     */
    public function __construct($details)
    {
        foreach ($details as $key => $detail) {
            $this->{$key} = $detail;
        }
    }
}